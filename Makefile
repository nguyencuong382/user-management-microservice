include .env

$(eval export $(shell sed -ne 's/ *#.*$//; /./ s/=.*$$// p' .env))

BUILD_IMAGE := $(DOCKER_REGISTRY_URL)-build:$(IMAGE_TAG)

env:
	echo $(BUILD_IMAGE)

docs:
	rm -rf ./src/user-microservice/docs
	swag init -d ./src/user-microservice -g Server.go -o ./src/user-microservice/docs

migrate:
	bash deploy/database/migrate_pg.sh

run:
	bash ./run.sh

test-local:
	bash ./test.sh

setup:
	docker-compose -f deploy/database/docker-compose.yml up -d
	make migrate

build:
	docker network inspect share_network >/dev/null 2>&1 || docker network create --driver bridge share_network
	docker build --no-cache -t $(BUILD_IMAGE) -f deploy/build/Dockerfile.build .
	docker build --no-cache --build-arg IMAGE=$(BUILD_IMAGE) -t $(DOCKER_REGISTRY_URL):$(IMAGE_TAG) -f deploy/build/Dockerfile .

test:
	docker-compose -f deploy/test/docker-compose.test.yml up --build --abort-on-container-exit user-microservice-test

deployment:
	docker-compose -f deploy/user-microservice/docker-compose.yml up -d --build --remove-orphans

clean:
	docker-compose -f deploy/database/docker-compose.yml down
	docker-compose -f deploy/user-microservice/docker-compose.yml down

	# docker rmi -f $(docker images -aq --filter dangling=true)
	# docker rmi -f $(BUILD_IMAGE)