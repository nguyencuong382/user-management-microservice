# User Management Microservice
## Contents
- [1. Background](#1.-background)

- [2. Requirements](#2.-requirements)
  - [2.1. User requirements](#2.1.-user-requirements)
  - [2.2. Functional requirements](#2.2.-functional-requirements)
  - [2.3. Non-functional requirements](#2.3.-non-functional-requirements)

- [3. Service design](#3.-service-design)
  - [3.1. System Overview](#3.1.-system-overview)
  - [3.2. User Management Backend design](#3.2.-user-management-backend-design)
  - [3.3. Database diagram](#3.3-database-diagram)
  - [3.4. Main class diagram](#3.4.-main-class-diagram)
  - [3.5. Sequence diagram](#3.5.-sequence-diagram)

- [4. User manual](#4.-user-manual)
  - [4.1. Setup environment](#4.1.-setup-environment)
  - [4.2. Deployment on local machine](#4.2.-Deployment-on-local-machine)
  - [4.3. Testing APIs](#4.3-Testing-APIs)
  - [4.4. Setup development project environment](#4.4.-setup-development-project-environment)
- [5. Other business/technical features of user management microservice](#5.-other-business-technical-features-of-user-management-microservice)

## 1. Background
Design and devlopment backend for `user management service` in a social network system running on local machine.

## 2. Requirements
### 2.1. User requirements

* CURD for users
* User schema has at least a username as a unique identifier.
* Service has database.
* Automation tests
* User manual for `build/test/deploy` on `local machine`
* Other business/technical features can have in the service.

### 2.2. Functional requirements

Service has only 2 actor `guest` and `admin`

* Allow `guest` to login, get JWT
* Allow `admin` to get user resource
  * Get users
  * Get an active user by username
  * Create a new user
  * Update an exisiting user
  * Delete an active user

#### 2.2.1 Business logic

* Maximum number of failed login attempts is 5
* Every APIs must be authorized (RBAC)

### 2.3. Non-functional requirements
* API
  * Uses `REST` interface
  * Max payload for single user record is < 1kB
  * Max payload for a resultset of 256kB
  * Compress response of APIs ([gzip](https://github.com/gin-contrib/gzip) or [brotli](https://github.com/anargu/gin-brotli))

* Performance
  * Service has `caching`

* Deployment
  * Use `docker` env for build/test/deploy

## 3. Service design
### 3.1. System Overview
<div align="center">
  <img src="images/overview-design.png"  width="500px" >
</div>

#### 3.1.1. Why Golang

* Blazing fast compile times
* Compiled language and `fast`
* Well-Scaled, Concurrency built-in, Goroutines take up only 2 kB of memory.

#### 3.1.2. Why Redis
The local machine doesn't have much more memory to store data for caching so we need a mechanism to ensure that data is still stored when the memory runs out. [Redis](https://redis.io/) can maintain data of a size bigger than its machine memory capacity. Another way to store data in cache is to use [Memcached](https://memcached.org/) but it is only a cache layer, not persistent, all data is stored in RAM which means it is difficult to backup data, and data can be lost.
Read more in here [Redis vs Mem](https://www.alibabacloud.com/blog/redis-vs--memcached%3A-in-memory-data-storage-systems_592091).
#### 3.1.3. Why Postgres
PostgreSQL isn't just relational, it's object-relational. This gives it some advantages over other open source SQL databases like MySQL, MariaDB and Firebird that Postgres can store nested and composite structures which standard RDBMS' don't support.
### 3.2. User Management Backend design
The service is designed based on [Domain Driven Design](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/ddd-oriented-microservice). Each microservice will be an context boundaries such as User context, Friend context, etc. DDD provides a pattern to easy plugin components, especially infrastructure components (change RMDB, change Message Queue, change communication protocol).
<div align="center">
<img src="images/user-management-backend-service.png"  width="300px" >
</div>

### 3.3. Database diagram
<div align="center">
<img src="images/database-design.png"  width="450px" >
</div>


### 3.4. Main class diagram
<div align="center">
<img src="images/class-diagram.png" >
</div>


### 3.5. Sequence diagram
<div align="center">
<img src="images/get-user-resource-sequence-diagram.png"  width="700px">
</div>

## 4. User Manual
### 4.1. Setup environment
1. Docker

```sh
# install_docker_and_tools
echo "Installing Docker..."
wget -qO- https://get.docker.com/ | sh
sudo usermod -a -G docker $USER 
chmod 777 /var/run/docker.sock
```

2. Docker Compose

```sh
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

### 4.2. Deployment on local machine

1. Clone project

```sh
$ git clone https://gitlab.com/nguyencuong382/user-management-microservice.git

# Go to project
$ cd ./user-management-microservice
```

2. Setup database

```sh
make setup
```

4. Build

```sh
make build
```

5. Test

```sh
make test
```

6. Deploy

```sh
make deployment
```

### 4.3. Testing APIs

#### 4.3.1. Testing with CURL & JD

1. Install curl & jd

```sh
sudo apt-get install curl jq
```

2. To access user resource APIs, you need to get JWT

```sh
export HOST=http://127.0.0.1:8002

curl -X POST $HOST/api/v1/auth -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"password\": \"cuongnm#11\", \"username\": \"cuongnm\"}" > /tmp/token.json
export ACCESS_TOKEN=$(cat /tmp/token.json | jq --raw-output '.token')
echo $ACCESS_TOKEN
```

```
# Output
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDA3MDI3ODYsIm9yaWdfaWF0IjoxNjAwNjk5MTg2LCJwZXJtaXNzaW9ucyI6WyJ1cGRhdGVfdXNlciIsImRlbGV0ZV91c2VyIiwiZ2V0X3VzZXJzIiwiY3JlYXRlX3VzZXIiLCJnZXRfdXNlciJdLCJyb2xlcyI6WyJhZG1pbiJdLCJ1c2VybmFtZSI6ImN1b25nbm0ifQ.1yZor4_YaYJQ88ppK-SZTXeeWz7sxtTockn5A3zmWUE
```

3. Sample Get Users APIs

```sh
curl -X GET "$HOST/api/v1/users?page=1" -H "accept: application/json" -H "Authorization: Bearer $ACCESS_TOKEN" | jq '.'
```

```json
{
  "totalPages": 101,
  "pageSize": 10,
  "count": 10,
  "data": [
    {
      "username": "aadamovitzhz",
      "first_name": "Agustin",
      "last_name": "Adamovitz",
      "phone": "747-264-6896",
      "email": "aadamovitzhz@bloomberg.com",
      "gender": "Male",
      "country": "France",
      "is_reported": null,
      "roles": null,
      "created_at": 1600749294,
      "update_at": null,
      "delete_at": null
    },
    ...
  ],
}
```

#### 4.3.2. Testing with swagger

1. Go to swagger page [http://localhost:8002/swagger/index.html](http://localhost:8002/swagger/index.html)
<div align="center">
<img src="images/swagger.png">
</div>

2. Gran JWT
<div align="center">
<img src="images/swagger-post-auth.png">
</div>
3. Set Authorize

* Click  ![images/swagger-auth-button.png](images/swagger-auth-button.png)

* Fill `Bearer [YOUR_JWT]` in **Value** text field then click **Authorize** button

<div align="center">
<img src="images/swagger-set-jwt.png">
</div>

4. Try APIs
\
After setup JWT for header, you can to go another APIs on Swagger UI for testing

### 4.4. Setup development project environment

1. Install glide

```sh
sudo apt-get install glide
```

2. Install packages
Make sure you are on current project folder `vinid-interview`

```sh
# Set GOPATH
export GOPATH=`pwd`

# Install go validator package
go get -v github.com/go-playground/validator
cd $GOPATH/src/github.com/go-playground/validator
ln -s . v10

# Install glide packages
cd $GOPATH/src/user-microservice
glide i
```

3. Config server

* There are 2 main environments, `local` env for development on local machine and `prod` for production environment deployed by docker-compose. There are also 2 test corresponding envs `test.local` and `test.prod`

* All config files are on `conf/user-microservice` folder

```ini
# conf/user-microservice/app.local.ini
# Service configuration sample
[server]
RunMode = debug
HTTPPort = 8000
ReadTimeout = 60
WriteTimeout = 60
ShutDownTimeOut = 15
```

* Make sure `redis` and `postgre` services are running and the configuration file `conf/user-microservice/app.local.ini` are correct
\
You can check database services by `docker` command

```sh
docker ps
```

* Run server

```sh
make run
```
## 5. Other business-technical features of user management microservice
### 5.1 Auth service
* This service can separate into service: authentication service and user management service.
  * Authentication service runs independently to perform authen for other services, can use [gRPC](https://grpc.io/) for fast and `synchronous` communication.
  * User management to manage users not only for admin but also allow for a user to retrieve his/her own info, register new account, setup account, etc.
* When user updates or login, the service should push a notification to the user and save it for security, can use [RabbitMQ](https://www.rabbitmq.com/), [Apache Kafka](https://kafka.apache.org/), Redis as [Publisher-Subscriber Pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/publisher-subscriber) for `asynchronous` communication.
* Normally users will register an account by their email or phone number, the service will connect a channel for 2-factor authentication.
\
*Overview extending features for user management service:* 
<div align="center">
<img src="images/microservice-overview.png" width="550px">
</div>

*Sequence diagram for auth service on microservice:* 
<div align="center">
<img src="images/auth-sequence-diagram.png" width="700px">
</div>

## 6. Conventions

* [Golang coding convention](https://www.golangprograms.com/code-formatting-and-naming-conventions-in-golang.html)
* [REST API format](https://github.com/cryptlex/rest-api-response-format)