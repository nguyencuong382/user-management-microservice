#!/bin/bash

export HOST=http://localhost:8002

# Get JWT
curl -X POST $HOST/api/v1/auth -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"password\": \"cuongnm#11\", \"username\": \"cuongnm\"}" > /tmp/token.json
export ACCESS_TOKEN=$(cat /tmp/token.json | jq --raw-output '.token')
echo $ACCESS_TOKEN

# Get users
curl -X GET "$HOST/api/v1/users?page=1" -H "accept: application/json" -H "Authorization: Bearer $ACCESS_TOKEN" | jq '.'

# Get user by username
curl -X GET "$HOST/api/v1/users/cuongnm" -H "accept: application/json" -H "Authorization: Bearer $ACCESS_TOKEN"