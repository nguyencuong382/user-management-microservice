#!/bin/sh

# Setup enviroment variables
BASEDIR=$(dirname "$0")
PG_HOST=127.0.0.1
PGPASSWORD=o7kvEFsr7FTaBdX2
PG_USER=postgres
PG_IMAGE=postgres:11.5-alpine
POSTGRES_CONTAINER=$(docker ps | grep $PG_IMAGE | awk '{ print $1 }')
echo $POSTGRES_CONTAINER

# Wait for the postgres container to be available
RETRIES=10
until docker container exec -e PGPASSWORD=$PGPASSWORD -i $POSTGRES_CONTAINER psql -h $PG_HOST -U $PG_USER -c "select 1" > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
  echo "Waiting for postgres server, $((RETRIES--)) remaining attempts..."
  sleep 1
done

if [ $RETRIES -eq 0 ]; then
  echo "Cannot connect to postgres docker container" >>/dev/stderr
  exit 1
fi

migrate () {
  echo "SQL file: $1"
  docker container exec -e PGPASSWORD=$PGPASSWORD -i $POSTGRES_CONTAINER psql -h postgres -U postgres < $1
  # docker run --network=share_network -i --rm \
  #   -e PGPASSWORD=$PGPASSWORD \
  #   postgres:11.5-alpine \
  #   psql -h postgres -U postgres < $SQL_FILE
}

# Migrate data to master database
echo "=== Migrate master database ==="
migrate $BASEDIR/user_micro.sql

# Migrate data to test database
echo "=== Migrate test database ==="
migrate $BASEDIR/user_micro_test.sql