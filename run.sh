#!/bin/bash

FILE=./bin/app

if test -f "$FILE"; then
  rm $FILE
fi

export ENV=$1

export GOPATH=`pwd`
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
export PATH_TO_CONFIG=./conf/user-microservice
go build -o $FILE ./src/user-microservice

if test -f "$FILE"; then
  exec $FILE
fi