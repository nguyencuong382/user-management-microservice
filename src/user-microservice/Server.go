package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"user-microservice/aggregator/infra/pg"
	"user-microservice/interface/routers"
	"user-microservice/utils/logger"
	"user-microservice/utils/setting"

	"github.com/gin-gonic/gin"
)

func init() {
	setting.Setup()
	logger.Setup()
}

// @title User Management Microservice API Document
// @version 1.0
// @description List APIs of Auth Microservice
// @termsOfService http://swagger.io/terms/
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @BasePath /api/v1
func main() {
	db := pg.GetPool()
	defer db.Close()
	gin.SetMode(setting.ServerSetting.RunMode)

	routersInit := routers.InitRouter()
	readTimeout := setting.ServerSetting.ReadTimeout
	writeTimeout := setting.ServerSetting.WriteTimeout
	endPoint := fmt.Sprintf(":%d", setting.ServerSetting.HTTPPort)
	logger.Logger.Info(setting.ServerSetting.HTTPPort)

	maxHeaderBytes := 1 << 20

	server := &http.Server{
		Addr:           endPoint,
		Handler:        routersInit,
		ReadTimeout:    readTimeout,
		WriteTimeout:   writeTimeout,
		MaxHeaderBytes: maxHeaderBytes,
	}

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := server.ListenAndServe(); err != nil {
			logger.Logger.Errorf("Error in ListenAndServe: %s", err)
		}
	}()

	logger.Logger.Infoln("Server is listening on: " + endPoint)
	logger.Logger.Infoln("Application started. Press CMD+C to shut down.")
	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), setting.ServerSetting.ShutDownTimeOut)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	server.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	logger.Logger.Infoln("Shutting down")
	os.Exit(0)

}
