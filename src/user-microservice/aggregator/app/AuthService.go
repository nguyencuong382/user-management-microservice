package app

import (
	"errors"
	"fmt"
	"strconv"
	"user-microservice/aggregator/domain/cache"
	"user-microservice/aggregator/domain/model"
	"user-microservice/aggregator/infra/pg"
	"user-microservice/aggregator/infra/redis"
	"user-microservice/utils"
	"user-microservice/utils/const/message"
	"user-microservice/utils/setting"

	"user-microservice/aggregator/infra/repo"

	"user-microservice/aggregator/domain/service"

	cst "user-microservice/utils/const"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
)

// Setup app
func Setup() {
	model.IRepoRepository = repo.NewRepo(pg.GetPool())
	model.IUserRepository = repo.NewAuthRepo(model.IRepoRepository, pg.GetPool())
	model.IRoleRepo = repo.NewRoleRepo(pg.GetPool())
	model.IUserPasswordRepository = repo.NewUserPasswordRepo(pg.GetPool())
	cache.ICache = redis.NewCacheRepository()
	Migrate()
}

// Authen godoc
// @Summary Check user authentication
// @Tags User
// @Accept json
// @Produce json
// @Param user body UserLoginDto true "Authentication input"
// @Success 200 {object} model.TokenDTO
// @Failure 400 {object} app.Message
// @Failure 401 {object} app.Message
// @Failure 403 {object} app.Message
// @Failure 500 {object} app.Message
// @Router /auth [post]
func Authen(c *gin.Context) (interface{}, error) {
	var param UserLoginDto
	c.BindJSON(&param)
	v := validator.New()
	if err := v.Struct(param); err != nil {
		return nil, err
	}

	user, err := findUserByUsername(param.Username)
	ok, err := utils.Exists(err)
	if err != nil {
		return nil, err
	}

	if !ok {
		return nil, errors.New(message.ERR_AUTH_USERNAME_PASSWORD_FAIL)
	}

	// Check user existing
	userPwd, err := model.IUserPasswordRepository.FindByUsername(user.Username)
	if err != nil {
		return nil, err
	}

	// Check if you tried to login many time
	keyCache := fmt.Sprintf("%s%s", cst.AUTH_FAIL_COUNT, user.Username)
	cacheStr, err := cache.ICache.Get(keyCache)
	remain := 0

	if err == nil {
		remain, _ = strconv.Atoi(cacheStr)
		if remain >= setting.AppSetting.MaxAuthFailed {
			return nil, errors.New(message.ERR_AUTH_USERNAME_PASSWORD_FAIL_TOO_MAX)
		}
	}

	ok = service.ComparePasswords(userPwd.Password, param.Password)
	if !ok {
		// Increase number of times user has tried to login
		cache.ICache.SetTimout(keyCache, remain+1, setting.AppSetting.TimeOutAuthFailed*60)
		return nil, errors.New(message.ERR_AUTH_USERNAME_PASSWORD_FAIL)
	}

	return user, nil
}
