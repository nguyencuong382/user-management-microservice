package app

import (
	"user-microservice/aggregator/domain/model"
	"user-microservice/aggregator/domain/service"
	"user-microservice/aggregator/infra/pg"
	"user-microservice/utils/logger"
	"user-microservice/utils/setting"
)

// Migrate migrate table and data to database
func Migrate() {
	if setting.AppSetting.MigrateTable {
		logger.Logger.Info("=== Migrating table ===")

		db := pg.GetPool()

		db.AutoMigrate(
			&model.User{},
			&model.Role{},
			&model.Permission{},
			&model.UserPassword{},
		)

		db.Table("user_passwords").AddForeignKey("user_username", "users(username)", "RESTRICT", "RESTRICT")
		db.Table("role_permissions").AddForeignKey("role_id", "roles(id)", "RESTRICT", "RESTRICT")
		db.Table("role_permissions").AddForeignKey("permission_id", "permissions(id)", "RESTRICT", "RESTRICT")
		db.Table("user_roles").AddForeignKey("user_username", "users(username)", "RESTRICT", "RESTRICT")
		db.Table("user_roles").AddForeignKey("role_id", "roles(id)", "RESTRICT", "RESTRICT")

		logger.Logger.Info("=== Finish migrating table ===")
	}

	if setting.AppSetting.MigrateData {
		logger.Logger.Info("=== Migrating data ===")

		migrateRolePermission()
		migrateUser()

		logger.Logger.Info("=== Finish migrating data ===")
	}

}

func migrateRolePermission() {
	role := model.Role{ID: "admin"}
	model.IRepoRepository.Save(&role)
	per := model.Permission{ID: "get_users"}
	model.IRepoRepository.Save(&per)
	model.IRoleRepo.AddPermission(&role, &per)
	per = model.Permission{ID: "create_user"}
	model.IRepoRepository.Save(&per)
	model.IRoleRepo.AddPermission(&role, &per)
	per = model.Permission{ID: "get_user"}
	model.IRepoRepository.Save(&per)
	model.IRoleRepo.AddPermission(&role, &per)
	per = model.Permission{ID: "update_user"}
	model.IRepoRepository.Save(&per)
	model.IRoleRepo.AddPermission(&role, &per)
	per = model.Permission{ID: "delete_user"}
	model.IRepoRepository.Save(&per)
	model.IRoleRepo.AddPermission(&role, &per)
}

func migrateUser() {
	hashPwd, err := service.HashAndSalt("cuongnm#11")
	if err == nil {
		email := "nguyencuong@gmail.com"
		phone := "035-555-666"
		user := model.User{Username: "cuongnm", Email: &email, FirstName: "Cuong", LastName: "Nguyen", Phone: &phone, Gender: "Male", Country: "Vietnam"}
		user.UserPasswords = []*model.UserPassword{
			{Password: string(hashPwd)},
		}
		model.IUserRepository.Create(&user)
		model.IUserRepository.AddRole(&user, &model.Role{ID: "admin"})
	}
}
