package app

// UserCreateDto : Input for Register service
type UserCreateDto struct {
	Username        string  `json:"username" example:"cuongnm3" validate:"required,min=6"`
	FirstName       string  `json:"first_name" example:"Cuong" validate:"required"`
	LastName        string  `json:"last_name" example:"Nguyen" validate:"required"`
	Gender          string  `json:"gender" example:"Male" validate:"required"`
	Country         string  `json:"country" example:"Vietnam" validate:"required"`
	Phone           *string `json:"phone" example:"949-344-0866" validate:"omitempty"`
	Email           *string `json:"email" example:"cuongnm3@gmail.com" validate:"omitempty,email"`
	Password        string  `json:"password" example:"cuongnm#12" validate:"required,max=50,min=6"`
	ConfirmPassword string  `json:"confirmPassword" example:"cuongnm#12" validate:"required,eqfield=Password"`
}
