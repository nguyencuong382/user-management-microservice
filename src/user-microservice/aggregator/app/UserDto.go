package app

import "user-microservice/aggregator/domain/model"

// UserDto represents the output of User dto
type UserDto struct {
	Username   string        `gorm:"primary_key" json:"username"`
	FirstName  string        `json:"first_name"`
	LastName   string        `json:"last_name"`
	Phone      *string       `json:"phone"`
	Email      *string       `json:"email"`
	Gender     string        `json:"gender"`
	Country    string        `json:"country"`
	IsReported *bool         `json:"is_reported"`
	Roles      []*model.Role `json:"roles"`
	model.Base
	DeletedAt *int `json:"delete_at"`
}
