package app

import (
	"user-microservice/aggregator/domain/model"

	"github.com/stroiman/go-automapper"
)

// CreateUserDto creates User Dto object from User object
func CreateUserDto(user *model.User) *UserDto {
	if user == nil {
		return nil
	}
	userDto := UserDto{}
	automapper.Map(user, &userDto)
	return &userDto
}

// CreateUsersDto creates Users Dto array from Users array
func CreateUsersDto(users []*model.User) []*UserDto {
	usersDto := make([]*UserDto, len(users))

	for i, u := range users {
		usersDto[i] = &UserDto{}
		automapper.Map(u, usersDto[i])

	}
	return usersDto
}
