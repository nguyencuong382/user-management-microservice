package app

// UserLoginDto represents the input for Login service
type UserLoginDto struct {
	Username string `json:"username" example:"cuongnm" validate:"required,max=50"`
	Password string `json:"password" example:"cuongnm#11" minimum:"6" validate:"required,max=50,min=6"`
}
