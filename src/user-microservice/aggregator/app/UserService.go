package app

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"user-microservice/aggregator/domain/cache"
	"user-microservice/aggregator/domain/model"
	"user-microservice/aggregator/domain/service"
	"user-microservice/utils"
	"user-microservice/utils/app"
	cst "user-microservice/utils/const"
	"user-microservice/utils/const/message"
	"user-microservice/utils/logger"
	"user-microservice/utils/setting"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
)

// FindAll godoc
// @Summary Get users
// @Tags Admin
// @Security ApiKeyAuth
// @Accept json
// @Produce json
// @Param page query int false "Page offset" default(1) maximum(1)
// @Success 200 {array} model.PagRes
// @Failure 400 {object} app.Message
// @Failure 401 {object} app.Message
// @Failure 403 {object} app.Message
// @Failure 500 {object} app.Message
// @Router /users [get]
func FindAll(c *gin.Context) {
	appG := app.Gin{C: c}
	data := app.Data{}
	pageOffset, err := strconv.Atoi(c.DefaultQuery("page", "1"))

	if err != nil || pageOffset < 1 {
		data.Code = http.StatusBadRequest
		data.Err = err
		appG.Response(&data)
		return
	}

	keyCache := fmt.Sprintf("%s%d", cst.FIND_ALL, pageOffset)
	cacheStr, err := cache.ICache.Get(keyCache)

	if err == nil {
		var got model.PagRes
		err = json.Unmarshal([]byte(cacheStr), &got)
		if err == nil {
			data.Data = got
			appG.Response(&data)
			return
		}
		logger.Logger.Error(err)
	}

	total, err := model.IRepoRepository.Count("users")

	if err != nil {
		data.Err = err
		appG.Response(&data)
		return
	}

	offset, take, totalPages := app.CalculateSkipOffset(pageOffset, total, setting.AppSetting.PageSize)
	items, _, err := model.IUserRepository.FindAll(offset, take)

	if err != nil {
		data.Err = err
		appG.Response(&data)
		return
	}

	data.Data = model.PagRes{
		Data:       CreateUsersDto(items),
		Count:      len(items),
		TotalPages: totalPages,
		PageSize:   setting.AppSetting.PageSize,
	}

	if len(items) > 0 {
		cache.ICache.Set(keyCache, data.Data)
	}

	data.Err = err
	appG.Response(&data)
}

// FindByUsername godoc
// @Summary Get user by username
// @Tags Admin
// @Security ApiKeyAuth
// @Accept json
// @Produce json
// @Param username path string true "Username" default(cuongnm)
// @Success 200 {object} app.UserDto
// @Failure 400 {object} app.Message
// @Failure 401 {object} app.Message
// @Failure 403 {object} app.Message
// @Failure 500 {object} app.Message
// @Router /users/{username} [get]
func FindByUsername(c *gin.Context) {
	appG := app.Gin{C: c}
	data := app.Data{}
	username := strings.TrimSpace(c.Param("username"))

	item, err := findUserByUsername(username)
	ok, err := utils.Exists(err)

	if err != nil {
		data.Err = err
		appG.Response(&data)
		return
	}

	if !ok {
		data.Msg = message.ERR_USER_NOT_FOUND
		data.Code = http.StatusNotFound
		appG.Response(&data)
		return
	}

	data.Data = CreateUserDto(item)
	appG.Response(&data)
}

// CreateUser godoc
// @Summary Create a new user
// @Tags Admin
// @Security ApiKeyAuth
// @Accept json
// @Produce json
// @Param user body UserCreateDto true "New User input"
// @Success 200 {object} app.UserDto
// @Failure 400 {object} app.Message
// @Failure 401 {object} app.Message
// @Failure 403 {object} app.Message
// @Failure 500 {object} app.Message
// @Router /users [post]
func CreateUser(c *gin.Context) {
	data := app.Data{}
	appG := app.Gin{C: c}
	var param UserCreateDto
	c.BindJSON(&param)
	v := validator.New()
	if err := v.Struct(param); err != nil {
		data.Err = err
		data.Code = http.StatusBadRequest
		appG.Response(&data)
		return
	}

	user, err := model.IUserRepository.FindByUsername(param.Username)
	ok, err := utils.Exists(err)
	if err != nil {
		fmt.Println(err)
		data.Err = err
		appG.Response(&data)
		return
	}

	if ok {
		data.Code = http.StatusBadRequest
		data.Msg = message.ERR_AUTH_ACCOUNT_EXSITS
		appG.Response(&data)
		return
	}

	hashPwd, err := service.HashAndSalt(param.Password)
	if err != nil {
		data.Err = err
		appG.Response(&data)
		return
	}

	user = &model.User{
		Username:  param.Username,
		Email:     param.Email,
		FirstName: param.FirstName,
		LastName:  param.LastName,
		Country:   param.Country,
		Phone:     param.Phone,
	}
	user.UserPasswords = []*model.UserPassword{
		{Password: string(hashPwd)},
	}
	err = model.IUserRepository.Create(user)
	if err != nil {
		data.Err = err
		appG.Response(&data)
		return
	}

	// Clear cache
	err = cache.ICache.Deletes(cst.FIND_ALL + "*")
	if err != nil {
		logger.Logger.Error(err)
	}

	data.Data = CreateUserDto(user)
	appG.Response(&data)
}

// UpdateUser godoc
// @Summary Update an existing user
// @Tags Admin
// @Security ApiKeyAuth
// @Accept json
// @Produce json
// @Param username path string true "Username"
// @Param user body UserUpdateDto true "Update user input"
// @Success 200 {object} app.UserDto
// @Failure 400 {object} app.Message
// @Failure 401 {object} app.Message
// @Failure 403 {object} app.Message
// @Failure 500 {object} app.Message
// @Router /users/{username} [put]
func UpdateUser(c *gin.Context) {
	data := app.Data{}
	appG := app.Gin{C: c}
	username := strings.TrimSpace(c.Param("username"))
	var param UserUpdateDto
	c.BindJSON(&param)
	v := validator.New()
	if err := v.Struct(param); err != nil {
		data.Err = err
		data.Code = http.StatusBadRequest
		appG.Response(&data)
		return
	}

	item, err := model.IUserRepository.FindByUsername(username)
	ok, err := utils.Exists(err)

	if err != nil {
		data.Err = err
		appG.Response(&data)
		return
	}

	if !ok {
		data.Msg = message.ERR_USER_NOT_FOUND
		data.Code = http.StatusNotFound
		appG.Response(&data)
		return
	}

	user := &model.User{Username: item.Username, Email: param.Email, Phone: param.Phone}
	if param.FirstName != nil {
		user.FirstName = *param.FirstName
	}
	if param.LastName != nil {
		user.FirstName = *param.LastName
	}
	if param.Country != nil {
		user.FirstName = *param.Country
	}
	err = model.IRepoRepository.Update(user)
	if err != nil {
		data.Err = err
		appG.Response(&data)
		return
	}

	// Clear cache
	err = cache.ICache.Deletes(cst.FIND_ALL + "*")
	err = cache.ICache.Delete(fmt.Sprintf("%s%s", cst.FIND_BY_USERNAME, username))
	if err != nil {
		logger.Logger.Error(err)
	}

	data.Data = CreateUserDto(user)
	appG.Response(&data)
}

// RemoveUser godoc
// @Summary Remove an existing user
// @Tags Admin
// @Security ApiKeyAuth
// @Accept json
// @Produce json
// @Param username path string true "Username"
// @Success 200 {object} app.Message
// @Failure 400 {object} app.Message
// @Failure 401 {object} app.Message
// @Failure 403 {object} app.Message
// @Failure 500 {object} app.Message
// @Router /users/{username} [delete]
func RemoveUser(c *gin.Context) {
	data := app.Data{}
	appG := app.Gin{C: c}
	username := strings.TrimSpace(c.Param("username"))

	item, err := model.IUserRepository.FindByUsername(username)
	ok, err := utils.Exists(err)

	if err != nil {
		data.Err = err
		appG.Response(&data)
		return
	}

	if !ok {
		data.Msg = message.ERR_USER_NOT_FOUND
		data.Code = http.StatusNotFound
		appG.Response(&data)
		return
	}

	user := model.User{Username: item.Username}
	err = model.IUserRepository.Remove(&user)
	if err != nil {
		data.Err = err
		appG.Response(&data)
		return
	}

	data.Data = &app.Message{
		Msg: "OK",
	}

	// Clear cache
	err = cache.ICache.Deletes(cst.FIND_ALL + "*")
	err = cache.ICache.Delete(fmt.Sprintf("%s%s", cst.FIND_BY_USERNAME, username))
	if err != nil {
		logger.Logger.Error(err)
	}

	appG.Response(&data)
}

func findUserByUsername(username string) (*model.User, error) {
	keyCache := fmt.Sprintf("%s%s", cst.FIND_BY_USERNAME, username)
	cacheStr, err := cache.ICache.Get(keyCache)

	if err == nil {
		var got model.User
		err = json.Unmarshal([]byte(cacheStr), &got)
		if err == nil {
			return &got, nil
		}
		logger.Logger.Error(err)
	}

	item, err := model.IUserRepository.FindByUsername(username, "Roles", "Roles.Permissions")
	if err != nil {
		return nil, err
	}

	cache.ICache.Set(keyCache, item)

	return item, nil
}
