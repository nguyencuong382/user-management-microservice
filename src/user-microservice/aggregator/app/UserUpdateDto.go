package app

// UserUpdateDto represents the input for Login service
type UserUpdateDto struct {
	FirstName *string `json:"first_name" example:"Cuong" `
	LastName  *string `json:"last_name" example:"Nguyen" `
	Country   *string `json:"country" example:"Vietnam"`
	Email     *string `json:"email" example:"example@gmail.com" validate:"omitempty,email"`
	Phone     *string `json:"phone" example:"949-344-0866" validate:"omitempty"`
}
