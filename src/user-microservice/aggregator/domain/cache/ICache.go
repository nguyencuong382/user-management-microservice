package cache

// ICache service
var ICache ICacheRepository

// ICacheRepository interface
type ICacheRepository interface {
	Set(key string, data interface{}) error
	SetTimout(key string, data interface{}, timeout int) error
	Get(key string) (string, error)
	Delete(key string) error
	Deletes(pattern string) error
}
