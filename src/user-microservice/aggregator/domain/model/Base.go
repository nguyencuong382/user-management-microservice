package model

import (
	"github.com/jinzhu/gorm"
	"github.com/kjk/betterguid"
)

// Base shape
type Base struct {
	CreatedAt int  `json:"created_at"`
	UpdatedAt *int `json:"update_at"`
}

// AddIDColumn generate uuid and add it to ID column
func AddIDColumn(scope *gorm.Scope) error {
	id := betterguid.New()
	return scope.SetColumn("ID", id)
}

// Pagination shape
type Pagination struct {
	Skip  int  `json:"skip"`
	Take  int  `json:"take"`
	Count bool `json:"count"`
	Data  bool `json:"data"`
}
