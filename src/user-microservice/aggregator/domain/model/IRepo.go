package model

// IRepoRepository repository
var IRepoRepository IRepo

// IRepo interface
type IRepo interface {
	Save(interface{}) error
	Update(interface{}) error
	Delete(interface{}) error
	Count(table string) (int, error)
}

// PagRes is Pagination Response
type PagRes struct {
	TotalPages int         `json:"totalPages"`
	PageSize   int         `json:"pageSize"`
	Count      int         `json:"count"`
	Data       interface{} `json:"data"`
}
