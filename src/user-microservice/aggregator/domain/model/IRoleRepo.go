package model

// IRoleRepo repository
var IRoleRepo IRoleRepository

// IRoleRepository interface
type IRoleRepository interface {
	AddPermission(*Role, *Permission) error
	RemovePermission(*Role, *Permission) error
}
