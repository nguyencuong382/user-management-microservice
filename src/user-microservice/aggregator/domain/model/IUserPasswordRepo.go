package model

// IUserPasswordRepository repository
var IUserPasswordRepository IUserPasswordRepo

// IUserPasswordRepo interface
type IUserPasswordRepo interface {
	FindByUsername(username string) (*UserPassword, error)
}
