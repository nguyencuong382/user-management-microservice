package model

// IUserRepository repository
var IUserRepository IUserRepo

// IUserRepo interface
type IUserRepo interface {
	FindAll(skip, take int) (items []*User, total int, err error)
	FindByUsername(username string, preloads ...string) (*User, error)
	UpdateFields(item *User, data map[string]interface{}) error
	AddRole(*User, *Role) error
	RemoveRole(*User, *Role) error
	Create(user *User) error
	Remove(*User) error
	Delete(*User) error
}
