package model

// Permission shape
type Permission struct {
	ID    string  `json:"id"`
	Roles []*Role `json:"-"`
}
