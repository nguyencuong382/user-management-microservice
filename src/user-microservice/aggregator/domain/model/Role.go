package model

// Role shape
type Role struct {
	ID          string        `json:"id"`
	Permissions []*Permission `gorm:"many2many:role_permissions;" json:"permissions"`
}
