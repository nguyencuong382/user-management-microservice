package model

// TokenDTO represents for GinJWTMiddleware
// Used ONLY for swagger document
type TokenDTO struct {
	Code    int    `json:"code"`
	Token   string `json:"token"`
	Expired int64  `json:"expired"`
}
