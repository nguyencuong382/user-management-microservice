package model

// TODO: add foreign key like this
// foreignkey:CommentID;association_foreignkey:CommentID

// User represent the model for an user
type User struct {
	Username      string          `gorm:"primary_key" json:"username"`
	FirstName     string          `json:"first_name"`
	LastName      string          `json:"last_name"`
	Phone         *string         `json:"phone"`
	Email         *string         `json:"email"`
	Gender        string          `json:"gender"`
	Country       string          `json:"country"`
	IsReported    *bool           `json:"is_reported"`
	UserPasswords []*UserPassword `gorm:"association_autoupdate:false"  json:"-"`
	Roles         []*Role         `gorm:"many2many:user_roles;association_autoupdate:false;association_autocreate:false;association_save_reference:false" json:"roles"`
	Base
	DeletedAt *int `json:"delete_at"`
}
