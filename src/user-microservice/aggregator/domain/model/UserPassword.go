package model

import "github.com/jinzhu/gorm"

// UserPassword represent the model for an password history of user
type UserPassword struct {
	ID           string `gorm:"primary_key" json:"id"`
	UserUsername string `json:"user_username"`
	Password     string `json:"-"`
	CreatedAt    int    `json:"created_at"`
	DeletedAt    *int   `json:"delete_at"`
}

// BeforeCreate update time before updating
func (m *UserPassword) BeforeCreate(scope *gorm.Scope) error {
	return AddIDColumn(scope)
}
