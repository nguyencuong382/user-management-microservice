package service

import (
	"golang.org/x/crypto/bcrypt"
)

// HashAndSalt creates password with salt
func HashAndSalt(pwd string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.MinCost)
}

// ComparePasswords compares hased with salt password with plain password
func ComparePasswords(hashedPwd string, plainPwd string) bool {
	byteString := []byte(plainPwd)
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, byteString)
	if err != nil {
		return false
	}
	return true
}
