package pg

import (
	"fmt"
	"sync"
	"user-microservice/aggregator/infra/database"
	"user-microservice/utils/setting"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var postgresPool *gorm.DB
var postgresPoolOnce sync.Once

// ConnString returns a connection string based on the parameters it's given
// This would normally also contain the password, however we're not using one
func ConnString(host string, port int, user string, dbName string, password string) string {
	return fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s sslmode=disable password=%s",
		host, port, user, dbName, password,
	)
}

// GetPool singleton grom DB
func GetPool() *gorm.DB {
	postgresPoolOnce.Do(func() {

		pgConfig := ConnString(
			setting.PostgresSetting.Host,
			setting.PostgresSetting.Port,
			setting.PostgresSetting.User,
			setting.PostgresSetting.DBName,
			setting.PostgresSetting.Password,
		)

		postgresPool = database.Setup("postgres", pgConfig)
	})
	return postgresPool
}
