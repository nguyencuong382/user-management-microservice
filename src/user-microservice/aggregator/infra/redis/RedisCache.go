package redis

import (
	"encoding/json"
	"errors"
	"fmt"
	"user-microservice/aggregator/domain/cache"
	"user-microservice/utils/setting"

	"github.com/garyburd/redigo/redis"
)

type repo struct {
	pool *redis.Pool
}

// NewCacheRepository implement InvoiceRepository
func NewCacheRepository() cache.ICacheRepository {
	return &repo{
		pool: GetPool(),
	}
}

func (r *repo) Set(key string, data interface{}) error {
	if !setting.AppSetting.Cache {
		return errors.New("Cache is turn off")
	}
	b, err := json.Marshal(data)
	if err != nil {
		return err
	}

	conn := r.pool.Get()
	defer conn.Close()

	_, err = conn.Do("SET", key, string(b))
	return err
}

func (r *repo) SetTimout(key string, data interface{}, timeout int) error {
	if !setting.AppSetting.Cache {
		return errors.New("Cache is turn off")
	}
	b, err := json.Marshal(data)
	if err != nil {
		return err
	}

	conn := r.pool.Get()
	defer conn.Close()

	_, err = conn.Do("SETEX", key, timeout, string(b))
	return err
}

func (r *repo) Get(key string) (string, error) {
	if !setting.AppSetting.Cache {
		return "", errors.New("Cache is turn off")
	}

	conn := r.pool.Get()
	defer conn.Close()

	objStr, err := redis.String(conn.Do("GET", key))
	return objStr, err
}

func (r *repo) Delete(key string) error {
	if !setting.AppSetting.Cache {
		return errors.New("Cache is turn off")
	}

	conn := r.pool.Get()
	defer conn.Close()

	_, err := conn.Do("DEL", key)
	return err
}

func (r *repo) Deletes(pattern string) error {
	if !setting.AppSetting.Cache {
		return errors.New("Cache is turn off")
	}

	conn := r.pool.Get()
	defer conn.Close()

	iter := 0
	keys := []string{}
	for {
		arr, err := redis.Values(conn.Do("SCAN", iter, "MATCH", pattern))
		if err != nil {
			return fmt.Errorf("error retrieving '%s' keys", pattern)
		}

		iter, _ = redis.Int(arr[0], nil)
		k, _ := redis.Strings(arr[1], nil)
		keys = append(keys, k...)

		if iter == 0 {
			break
		}
	}

	fmt.Print(keys)

	for _, k := range keys {
		_, err := conn.Do("DEL", k)
		if err != nil {
			return fmt.Errorf("error deleting '%s' key", k)
		}
	}

	return nil
}
