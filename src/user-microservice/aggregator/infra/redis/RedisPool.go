package redis

import (
	"strings"
	"sync"
	"user-microservice/utils/logger"
	"user-microservice/utils/setting"

	"github.com/garyburd/redigo/redis"
)

var redisPool *redis.Pool
var redisPoolOnce sync.Once

// GetPool singleton redis
func GetPool() *redis.Pool {
	redisPoolOnce.Do(func() {
		redisAddr := setting.RedisSetting.Host
		redisPwd := setting.RedisSetting.Password

		pool := &redis.Pool{
			MaxIdle:         setting.RedisSetting.MaxIdle,
			MaxActive:       setting.RedisSetting.MaxActive, // max number of connections
			IdleTimeout:     setting.RedisSetting.IdleTimeout,
			MaxConnLifetime: 0,
			Dial: func() (redis.Conn, error) {
				c, err := redis.Dial("tcp", redisAddr, redis.DialDatabase(setting.RedisSetting.DBNumber))
				if err != nil {
					logger.Logger.Errorf("Error in Initialize Redis Connection: %s", err)
					return nil, err
				}
				if strings.Compare(redisPwd, "") != 0 {
					if _, err := c.Do("AUTH", redisPwd); err != nil {
						c.Close()
						logger.Logger.Errorf("Error in Initialize Redis Connection: %s", err)
						return nil, err
					}
				}
				return c, err
			},
		}
		redisPool = pool
	})
	return redisPool
}

func Ping() {
	_, err := redis.String(GetPool().Get().Do("PING"))
	if err != nil {
		logger.Logger.Fatalf("Cannot connect to redis: %v", err)
		// panic(fmt.Sprintf("%v: %v", msg, err))
	} else {
		logger.Logger.Info("Ping redis successfully")
	}
}
