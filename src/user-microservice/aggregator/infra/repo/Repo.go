package repo

import (
	"user-microservice/aggregator/domain/model"

	"github.com/jinzhu/gorm"
)

// Repo struct
type Repo struct {
	db *gorm.DB
}

// NewRepo implement
func NewRepo(db *gorm.DB) model.IRepo {
	return &Repo{db: db}
}

// GetAll repo
func (r *Repo) GetAll(table string) ([]interface{}, error) {
	var items []interface{}
	err := r.db.Table(table).Find(&items).Error
	return items, err
}

// FirstOrCreate repo
func (r *Repo) FirstOrCreate(item interface{}) error {
	return r.db.FirstOrCreate(item).Error
}

// Save repo
func (r *Repo) Save(item interface{}) error {
	return r.db.Create(item).Error
}

// Update repo
func (r *Repo) Update(item interface{}) error {
	return r.db.Model(item).Update(item).Error
}

// Delete repo
func (r *Repo) Delete(item interface{}) error {
	return r.db.Delete(item).Error
}

// Count repo
func (r *Repo) Count(table string) (int, error) {
	var count int
	err := r.db.Table(table).Count(&count).Error
	return count, err
}
