package repo

import (
	"user-microservice/aggregator/domain/model"

	"github.com/jinzhu/gorm"
)

type repo struct {
	db *gorm.DB
}

// NewRoleRepo implement
func NewRoleRepo(db *gorm.DB) model.IRoleRepository {
	return &repo{db: db}
}

func (r *repo) AddPermission(role *model.Role, per *model.Permission) error {
	return r.db.Model(role).Association("Permissions").Append(per).Error

}

func (r *repo) RemovePermission(role *model.Role, per *model.Permission) error {
	return r.db.Model(role).Association("Permissions").Delete(per).Error
}
