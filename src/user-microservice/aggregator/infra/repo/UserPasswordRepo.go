package repo

import (
	"user-microservice/aggregator/domain/model"

	"github.com/jinzhu/gorm"
)

type userPasswordRepo struct {
	db *gorm.DB
}

// NewUserPasswordRepo implement
func NewUserPasswordRepo(db *gorm.DB) model.IUserPasswordRepo {
	return &userPasswordRepo{db: db}
}

func (r *userPasswordRepo) FindByUsername(username string) (*model.UserPassword, error) {
	var item model.UserPassword
	err := r.db.Where("user_username = ?", username).First(&item).Error
	return &item, err
}
