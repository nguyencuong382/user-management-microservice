package repo

import (
	"user-microservice/aggregator/domain/model"

	"github.com/jinzhu/gorm"
)

type autRepo struct {
	model.IRepo
	db *gorm.DB
}

// NewAuthRepo implement InvoiceRepository
func NewAuthRepo(baseRepo model.IRepo, db *gorm.DB) model.IUserRepo {
	return &autRepo{IRepo: baseRepo, db: db}
}

func (r *autRepo) FindAll(skip, take int) (items []*model.User, total int, err error) {
	err = r.db.Table("users").Unscoped().Order("username").Offset(skip).Limit(take).Find(&items).Error
	return items, total, err
}

func (r *autRepo) FindByUsername(username string, preloads ...string) (*model.User, error) {
	var item model.User
	db := r.db.Where("username = ?", username)
	for _, preload := range preloads {
		db = db.Preload(preload)
	}
	err := db.First(&item).Error
	if err != nil {
		return nil, err
	}
	return &item, nil
}

// Update Utter - Override default update
func (r *autRepo) UpdateFields(item *model.User, data map[string]interface{}) error {
	return r.db.Table("users").Where("username = ?", item.Username).Updates(data).Error
}

// Update Utter - Override default update
func (r *autRepo) Remove(item *model.User) error {
	return r.db.Delete(item).Error
}

// Update Utter - Override default update
func (r *autRepo) Delete(item *model.User) error {
	tx := r.db.Begin()

	err := tx.Model(item).Association("Roles").Clear().Error
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Exec("delete from user_passwords where user_username = ?", item.Username).Error

	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Exec("delete from users where username = ?", item.Username).Error

	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit().Error
	if err != nil {
		tx.Rollback()
		return err
	}
	return nil
}

// AddRole Utter - Override default update
func (r *autRepo) Create(user *model.User) error {
	return r.db.Create(user).Error
}

// AddRole Utter - Override default update
func (r *autRepo) AddRole(user *model.User, role *model.Role) error {
	return r.db.Model(user).Association("Roles").Append(role).Error
}

// Update Utter - Override default update
func (r *autRepo) RemoveRole(user *model.User, role *model.Role) error {
	return r.db.Model(user).Association("Roles").Delete(role).Error
}
