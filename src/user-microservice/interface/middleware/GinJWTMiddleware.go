package middleware

import (
	"time"
	"user-microservice/aggregator/app"
	"user-microservice/aggregator/domain/model"
	utilapp "user-microservice/utils/app"
	"user-microservice/utils/logger"
	"user-microservice/utils/setting"

	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
)

// NewGinJWTMiddleware create new GinJWTMiddleware
func NewGinJWTMiddleware() *jwt.GinJWTMiddleware {
	// the jwt middleware
	identityKey := "username"
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "test zone",
		Key:         []byte(setting.AppSetting.JwtSecret),
		Timeout:     time.Duration(setting.AppSetting.JWTExpiredTime) * time.Hour,
		MaxRefresh:  time.Hour,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*model.User); ok {
				setRoles := make(map[string]bool)
				setPers := make(map[string]bool)

				for _, role := range v.Roles {
					setRoles[role.ID] = true
					for _, per := range role.Permissions {
						setPers[per.ID] = true
					}
				}

				roles := []string{}

				for role := range setRoles {
					roles = append(roles, role)
				}

				pers := []string{}

				for per := range setPers {
					pers = append(pers, per)
				}

				return jwt.MapClaims{
					identityKey:   v.Username,
					"roles":       roles,
					"permissions": pers,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &model.User{
				Username: claims[identityKey].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			return app.Authen(c)
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if _, ok := data.(*model.User); ok {
				return true
			}
			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, utilapp.Message{Msg: message})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})

	if err != nil {
		logger.Logger.Fatal("JWT Error:" + err.Error())
	}

	errInit := authMiddleware.MiddlewareInit()

	if errInit != nil {
		logger.Logger.Fatal("authMiddleware.MiddlewareInit() Error:" + errInit.Error())
	}

	return authMiddleware
}
