package middleware

import (
	"net/http"
	"strings"
	"user-microservice/utils/const/message"

	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
)

// RBAC middleware
func RBAC(per string) gin.HandlerFunc {
	return func(c *gin.Context) {
		claims := jwt.ExtractClaims(c)

		pers := claims["permissions"].([]interface{})

		for _, perStr := range pers {
			if strings.Compare(perStr.(string), per) == 0 {
				c.Next()
				return
			}
		}

		c.JSON(http.StatusForbidden, message.ERR_JWT_FORBIDDEN)
		c.Abort()
	}
}
