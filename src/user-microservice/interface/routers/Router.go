package routers

import (
	"user-microservice/aggregator/app"
	"user-microservice/interface/middleware"

	_ "user-microservice/docs"

	brotli "github.com/anargu/gin-brotli"
	"github.com/gin-gonic/gin"

	"github.com/gin-contrib/cors"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// InitRouter initialize routing information
func InitRouter() *gin.Engine {
	app.Setup()
	r := gin.New()
	r.Use(middleware.LoggerToFile())
	r.Use(brotli.Brotli(brotli.DefaultCompression))
	r.Use(gin.Recovery())
	r.Use(cors.New(cors.Config{
		// AllowOrigins:     []string{"http://localhost:3000"},
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "POST", "GET", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Accept", "Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization"},
		AllowCredentials: true,
	}))

	authMiddleware := middleware.NewGinJWTMiddleware()

	r.GET("/health-check", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"status": "ok",
		})
	})

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	apiv1 := r.Group("/api/v1")
	apiv1.POST("/auth", authMiddleware.LoginHandler)
	apiv1.Use(authMiddleware.MiddlewareFunc())
	{
		userAPI := apiv1.Group("/users")
		{
			userAPI.GET("", middleware.RBAC("get_users"), app.FindAll)
			userAPI.GET("/:username", middleware.RBAC("get_user"), app.FindByUsername)
			userAPI.POST("", middleware.RBAC("create_user"), app.CreateUser)
			userAPI.PUT("/:username", middleware.RBAC("update_user"), app.UpdateUser)
			userAPI.DELETE("/:username", app.RemoveUser)
		}
	}

	return r
}
