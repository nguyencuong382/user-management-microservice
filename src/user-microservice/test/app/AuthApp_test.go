package app

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http/httptest"
	"user-microservice/aggregator/domain/model"
	"user-microservice/interface/routers"
	"user-microservice/test/helper"
	"user-microservice/utils/logger"
	"user-microservice/utils/setting"

	"github.com/gin-gonic/gin"
)

var router *gin.Engine

func init() {
	setting.Setup()
	logger.Setup()
	router = routers.InitRouter()
	Setup()
}

func Setup() {
	token, err := GetToken(map[string]interface{}{
		"username": "cuongnm",
		"password": "cuongnm#11",
	})
	if err != nil {
		logger.Logger.Fatal(err)
	}
	setting.AuthTestSetting.AdminJWT = token.Token
}

func GetToken(input map[string]interface{}) (*model.TokenDTO, error) {
	ts := httptest.NewServer(router)

	defer ts.Close()

	testCase := helper.TestMeasure{
		Input: (&input),
	}

	res, err := helper.Post(fmt.Sprintf("%s/api/v1/auth", ts.URL), testCase)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != 200 {
		return nil, errors.New("Cannot get jwt for actors. Using default config loaded from file config")
	}

	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return nil, err
	}
	var token *model.TokenDTO
	err = json.Unmarshal(body, &token)

	if err != nil {
		return nil, err
	}

	return token, err
}
