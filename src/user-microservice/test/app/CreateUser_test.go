package app

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"user-microservice/aggregator/domain/model"
	"user-microservice/test/helper"
	"user-microservice/utils/setting"

	"github.com/stretchr/testify/assert"
)

// TestCreateUser ..
func TestCreateUser(t *testing.T) {
	user := model.User{
		Username: "torys4test",
	}
	model.IUserRepository.Delete(&user)

	ts := httptest.NewServer(router)

	defer ts.Close()

	result := helper.TestResult{
		Name: "TestCreateUser",
	}

	var testCases []helper.TestMeasure

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{
			"username":        user.Username,
			"first_name":      "test",
			"last_name":       "test",
			"password":        user.Username,
			"confirmPassword": user.Username,
			"email":           user.Username + "@gmail.com",
			"country":         "Vietname",
			"gender":          "Male",
		}),
		Expected: http.StatusOK,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Expected: http.StatusUnauthorized,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token:    setting.AuthTestSetting.AdminJWT,
		Input:    (&map[string]interface{}{}),
		Expected: http.StatusBadRequest,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{
			"email": "nguyencuong@gmail.com",
		}),
		Expected: http.StatusBadRequest,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{
			"email": "nguyencuong @gmail.com",
		}),
		Expected: http.StatusBadRequest,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{
			"username":        "cuong",
			"password":        "12",
			"confirmPassword": "12",
		}),
		Expected: http.StatusBadRequest,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{
			"username":        "cuong",
			"password":        "cuong$121212",
			"confirmPassword": "cuong$test",
		}),
		Expected: http.StatusBadRequest,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{
			"username":        user.Username,
			"password":        "cuongnmx321",
			"confirmPassword": "cuongnmx321",
		}),
		Expected: http.StatusBadRequest,
		Assert:   "StatusCode",
	})

	for _, testCase := range testCases {
		res, err := helper.Post(fmt.Sprintf("%s/api/v1/users", ts.URL), testCase)
		helper.MeasureFunc(t, res, err, &result, &testCase)
	}

	testCase := helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: user.Username,
			},
		},
		Expected: http.StatusBadRequest,
		Assert:   "StatusCode",
	}

	res, err := helper.Get(fmt.Sprintf("%s/api/v1/users/:username", ts.URL), testCase)

	if err != nil {
		assert.Equal(t, err, nil)
		return
	}
	if assert.Equal(t, http.StatusOK, res.StatusCode) {
		body, errr := ioutil.ReadAll(res.Body)
		if errr == nil {
			want := model.User{
				Username: user.Username,
			}
			var got model.User

			_ = json.Unmarshal([]byte(body), &got)
			got = model.User{
				Username: got.Username,
			}
			assert.Equal(t, got, want)
		}
	}

	helper.PrintResult(result)
}
