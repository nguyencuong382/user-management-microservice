package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"user-microservice/aggregator/domain/model"
	"user-microservice/test/helper"
	"user-microservice/utils/setting"
)

// TestDeleteUser ..
func TestDeleteUser(t *testing.T) {
	user := model.User{
		Username: "grottger6",
	}
	model.IUserRepository.UpdateFields(&user, map[string]interface{}{"deleted_at": nil})

	ts := httptest.NewServer(router)

	defer ts.Close()

	result := helper.TestResult{
		Name: "TestCreateUser",
	}

	testCase := helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: user.Username,
			},
		},
		Expected: http.StatusOK,
		Assert:   "StatusCode",
	}

	res, err := helper.Get(fmt.Sprintf("%s/api/v1/users/:username", ts.URL), testCase)
	helper.MeasureFunc(t, res, err, &result, &testCase)

	var testCases []helper.TestMeasure

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "xxxxxx",
			},
		},
		Expected: http.StatusNotFound,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "xxxxxx",
			},
		},
		Expected: http.StatusNotFound,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: user.Username,
			},
		},
		Expected: http.StatusOK,
		Assert:   "StatusCode",
	})

	for _, testCase := range testCases {
		res, err := helper.Delete(fmt.Sprintf("%s/api/v1/users/:username", ts.URL), testCase)
		helper.MeasureFunc(t, res, err, &result, &testCase)
	}

	// testCase = helper.TestMeasure{
	// 	Token: setting.AuthTestSetting.AdminJWT,
	// 	Params: []helper.Property{
	// 		{
	// 			Key:   "username",
	// 			Value: user.Username,
	// 		},
	// 	},
	// 	Expected: http.StatusNotFound,
	// 	Assert:   "StatusCode",
	// }

	// res, err = helper.Get(fmt.Sprintf("%s/api/v1/users/:username", ts.URL), testCase)
	// helper.MeasureFunc(t, res, err, &result, &testCase)

	helper.PrintResult(result)
}
