package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"user-microservice/test/helper"
	"user-microservice/utils/logger"
	"user-microservice/utils/setting"
)

func TestGetAll(t *testing.T) {
	ts := httptest.NewServer(router)

	defer ts.Close()

	result := helper.TestResult{
		Name: "TestGetAll",
	}

	logger.Logger.Info(setting.TestSetting.URL)

	var testCases []helper.TestMeasure

	testCases = append(testCases, helper.TestMeasure{
		Expected: http.StatusUnauthorized,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token:    setting.AuthTestSetting.AdminJWT,
		Expected: http.StatusOK,
		Assert:   "StatusCode",
	})

	for _, testCase := range testCases {
		res, err := helper.Get(fmt.Sprintf("%s/api/v1/users", ts.URL), testCase)
		helper.MeasureFunc(t, res, err, &result, &testCase)
	}

	helper.PrintResult(result)
}
