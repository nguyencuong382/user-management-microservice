package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"user-microservice/test/helper"
	"user-microservice/utils/setting"
)

func TestGetByUsername(t *testing.T) {
	ts := httptest.NewServer(router)

	defer ts.Close()

	result := helper.TestResult{
		Name: "TestGetByUsername",
	}

	var testCases []helper.TestMeasure

	testCases = append(testCases, helper.TestMeasure{
		Expected: http.StatusUnauthorized,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "cuongnm",
			},
		},
		Expected: http.StatusOK,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "cuongnm!test",
			},
		},
		Expected: http.StatusNotFound,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "cuongnm or 1=1",
			},
		},
		Expected: http.StatusNotFound,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "xx' or '1'='1",
			},
		},
		Expected: http.StatusNotFound,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "2' or '1",
			},
		},
		Expected: http.StatusNotFound,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: " ",
			},
		},
		Expected: http.StatusNotFound,
		Assert:   "StatusCode",
	})

	for _, testCase := range testCases {
		res, err := helper.Get(fmt.Sprintf("%s/api/v1/users/:username", ts.URL), testCase)
		helper.MeasureFunc(t, res, err, &result, &testCase)
	}

	helper.PrintResult(result)
}
