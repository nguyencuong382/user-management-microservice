package app

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"user-microservice/aggregator/domain/model"
	"user-microservice/test/helper"
	"user-microservice/utils/setting"

	"github.com/stretchr/testify/assert"
)

// TestUpdateUser ..
func TestUpdateUser(t *testing.T) {
	ts := httptest.NewServer(router)

	defer ts.Close()

	result := helper.TestResult{
		Name: "TestUpdateUser",
	}

	var testCases []helper.TestMeasure

	testCases = append(testCases, helper.TestMeasure{
		Expected: http.StatusUnauthorized,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{}),
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "cuongnmxxx",
			},
		},
		Expected: http.StatusNotFound,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{
			"email": "nguyencuong@gmail.com",
		}),
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "cuongnmxx",
			},
		},
		Expected: http.StatusNotFound,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{
			"email": "nguyencuong @gmail.com",
		}),
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "cuongnm",
			},
		},
		Expected: http.StatusBadRequest,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{
			"email": "nguyencuong",
		}),
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "cuongnm",
			},
		},
		Expected: http.StatusBadRequest,
		Assert:   "StatusCode",
	})

	testCases = append(testCases, helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Input: (&map[string]interface{}{
			"email": "nguyencuong@gmail.com",
		}),
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "cuongnm",
			},
		},
		Expected: http.StatusOK,
		Assert:   "StatusCode",
	})

	for _, testCase := range testCases {
		res, err := helper.Put(fmt.Sprintf("%s/api/v1/users/:username", ts.URL), testCase)
		helper.MeasureFunc(t, res, err, &result, &testCase)
	}

	testCase := helper.TestMeasure{
		Token: setting.AuthTestSetting.AdminJWT,
		Params: []helper.Property{
			{
				Key:   "username",
				Value: "cuongnm",
			},
		},
		Expected: http.StatusBadRequest,
		Assert:   "StatusCode",
	}

	res, err := helper.Get(fmt.Sprintf("%s/api/v1/users/:username", ts.URL), testCase)

	if err != nil {
		assert.Equal(t, err, nil)
		return
	}
	if assert.Equal(t, http.StatusOK, res.StatusCode) {
		body, errr := ioutil.ReadAll(res.Body)
		if errr == nil {
			email := "nguyencuong@gmail.com"
			want := model.User{
				Username: "cuongnm",
				Email:    &email,
			}
			var got model.User

			_ = json.Unmarshal([]byte(body), &got)
			got = model.User{
				Username: got.Username,
				Email:    got.Email,
			}
			assert.Equal(t, got, want)
		}
	}

	helper.PrintResult(result)
}
