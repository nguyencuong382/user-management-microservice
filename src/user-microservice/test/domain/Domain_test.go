package app

import (
	"user-microservice/aggregator/app"
	"user-microservice/utils/logger"
	"user-microservice/utils/setting"
)

func init() {
	setting.Setup()
	logger.Setup()
	app.Setup()
}
