package app

import (
	"testing"
	"user-microservice/aggregator/domain/model"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestGetByUsername(t *testing.T) {
	got, err := model.IUserRepository.FindByUsername("")
	assert.Equal(t, gorm.ErrRecordNotFound, err)
	var want *model.User
	assert.Equal(t, want, got)

	got, err = model.IUserRepository.FindByUsername("xxx")
	assert.Equal(t, gorm.ErrRecordNotFound, err)
	assert.Equal(t, want, got)

	got, err = model.IUserRepository.FindByUsername("cuongnm or 1=1")
	assert.Equal(t, gorm.ErrRecordNotFound, err)
	assert.Equal(t, want, got)

	got, err = model.IUserRepository.FindByUsername("xx' or '1'='1")
	assert.Equal(t, gorm.ErrRecordNotFound, err)
	assert.Equal(t, want, got)

	got, err = model.IUserRepository.FindByUsername("2' or '1")
	assert.Equal(t, gorm.ErrRecordNotFound, err)
	assert.Equal(t, want, got)

	got, err = model.IUserRepository.FindByUsername("cuongnm")
	want = &model.User{
		Username: "cuongnm",
	}
	got = &model.User{
		Username: got.Username,
	}
	assert.Equal(t, nil, err)
	assert.Equal(t, want, got)

	got, err = model.IUserRepository.FindByUsername("mdiprose1")
	want = &model.User{
		Username: "mdiprose1",
	}
	got = &model.User{
		Username: got.Username,
	}
	assert.Equal(t, nil, err)
	assert.Equal(t, want, got)
}
