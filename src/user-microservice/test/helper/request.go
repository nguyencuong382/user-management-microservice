package helper

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/url"
	"strings"
)

func makeRequest(method string, url string, data *map[string]interface{}, token string, params *[]Property) (*http.Response, error) {

	client := http.Client{}

	var buffer *bytes.Buffer = nil

	// fmt.Println("data:", data, "buffer:", buffer)
	if data != nil {
		body, err := json.Marshal(*data)
		if err != nil {
			return nil, err
		}
		buffer = bytes.NewBuffer(body)
	}

	endpoint := url

	// check parameters
	if params != nil {
		for _, p := range *params {
			endpoint = strings.Replace(endpoint, ":"+p.Key, p.Value, 1)
		}
	}

	req, err := http.NewRequest(method, endpoint, buffer)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	if token != "" {
		req.Header.Set("Authorization", "Bearer "+token)
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func makeTestRequest(method string, _link string, testCase TestMeasure) (*http.Response, error) {

	client := http.Client{}

	var buffer *bytes.Buffer = nil

	// check body data
	if testCase.Input != nil {
		body, err := json.Marshal(*testCase.Input)
		if err != nil {
			return nil, err
		}
		buffer = bytes.NewBuffer(body)
	} else {
		buffer = &bytes.Buffer{}
	}

	endpoint := _link

	// check parameters
	if len(testCase.Params) > 0 {
		for _, p := range testCase.Params {
			endpoint = strings.Replace(endpoint, ":"+p.Key, p.Value, -1)
		}
	}

	u, _ := url.Parse(endpoint)

	// check queries
	if len(testCase.Queries) > 0 {
		query := u.Query()
		for _, p := range testCase.Queries {
			query.Set(p.Key, p.Value)
		}

		u.RawQuery = query.Encode()
	}

	req, err := http.NewRequest(method, u.String(), buffer)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	if testCase.Token != "" {
		req.Header.Set("Authorization", "Bearer "+testCase.Token)
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}

// Get method
func Get(url string, testCase TestMeasure) (*http.Response, error) {
	return makeTestRequest("GET", url, testCase)
}

// Post method
func Post(url string, testCase TestMeasure) (*http.Response, error) {
	return makeTestRequest("POST", url, testCase)
}

// Put method
func Put(url string, testCase TestMeasure) (*http.Response, error) {
	return makeTestRequest("PUT", url, testCase)
}

// Delete method
func Delete(url string, testCase TestMeasure) (*http.Response, error) {
	return makeTestRequest("DELETE", url, testCase)
}

// PurePost method
func PurePost(url string, data *map[string]interface{}, token string) (*http.Response, error) {
	return makeRequest("POST", url, data, token, nil)
}
