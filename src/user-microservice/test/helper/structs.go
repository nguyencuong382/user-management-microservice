package helper

// TestResult ...
type TestResult struct {
	Name     string
	Success  int
	Fail     int
	Errors   []string
	FailedAt []int
}

// Total test cases
func (m TestResult) Total() int {
	return m.Success + m.Fail
}

// TestMeasure ...
type TestMeasure struct {
	Params      []Property
	Queries     []Property
	Input       *map[string]interface{}
	Token       string
	Expected    interface{}
	Got         interface{}
	Assert      string
	Err         error
	InputString string
}

// Property ...
type Property struct {
	Key   string
	Value string
}
