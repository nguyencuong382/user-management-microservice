package helper

import (
	"io/ioutil"
	"net/http"
	"strconv"
	"testing"
	"time"
	"user-microservice/utils/logger"
)

// DebugColor, ErrorColor, InfoColor, NoticeColor, WarningColor
const (
	InfoColor    = "\033[1;34m%s\033[0m"
	NoticeColor  = "\033[1;36m%s\033[0m"
	WarningColor = "\033[1;33m%s\033[0m"
	ErrorColor   = "\033[1;31m%s\033[0m"
	DebugColor   = "\033[0;36m%s\033[0m"
)

// TimeUnixString ..
func TimeUnixString() string {
	return strconv.FormatInt(time.Now().UnixNano(), 10)
}

// PrintResults ...
func PrintResults(results []TestResult) {
	for _, result := range results {
		PrintResult(result)
	}
}

// PrintResult ...
func PrintResult(r TestResult) {
	logger.Logger.Info("[", r.Name, "]: ", r.Success, " successes, ", r.Fail, " fails.")
}

// CountTests ...
func CountTests(testCases []TestResult) TestResult {
	output := TestResult{
		Fail:    0,
		Success: 0,
	}
	for _, testCase := range testCases {
		output.Fail += testCase.Fail
		output.Success += testCase.Success
	}
	return output
}

// MeasureFunc ...
func MeasureFunc(t *testing.T, res *http.Response, err error, result *TestResult, testCase *TestMeasure) {
	if err != nil {
		(*result).Fail++
		// (*result).Errors = append((*result).Errors, err.Error())
		logger.Logger.Error(err)
		t.Error(err)
		return
	}

	if testCase.Assert == "StatusCode" {
		if res.StatusCode == testCase.Expected {
			(*result).Success++
		} else {
			(*result).Fail++
			bodyBytes, _ := ioutil.ReadAll(res.Body)
			bodyString := string(bodyBytes)
			logger.Logger.Error(bodyString)
			t.Errorf("Expected <%d> got <%d>", testCase.Expected, res.StatusCode)
			// (*result).Errors = append((*result).Errors, fmt.Sprint("Expected <", testCase.Expected, "> got <", res.StatusCode, ">"))
		}
	}
}

// MeasureFuncIT ...
func MeasureFuncIT(t *testing.T, err error, result *TestResult, testCase *TestMeasure) {
	if err != nil {
		(*result).Fail++
		// (*result).Errors = append((*result).Errors, err.Error())
		t.Error(err)
		return
	}

	// if testCase.Assert == "StatusCode" {
	// 	if res.StatusCode == testCase.Expected {
	// 		(*result).Success++
	// 	} else {
	// 		(*result).Fail++
	// 		t.Errorf("Expected <%d> got <%d>", testCase.Expected, res.StatusCode)
	// 		// (*result).Errors = append((*result).Errors, fmt.Sprint("Expected <", testCase.Expected, "> got <", res.StatusCode, ">"))
	// 	}
	// }
}
