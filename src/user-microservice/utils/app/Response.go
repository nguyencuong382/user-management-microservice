package app

import (
	"net/http"
	"user-microservice/utils/const/message"
	"user-microservice/utils/logger"

	"github.com/gin-gonic/gin"
)

// Gin struct
type Gin struct {
	C *gin.Context
}

// Response struct
type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// Data response param
type Data struct {
	Code int
	Msg  string
	Err  error
	Errs []error
	Data interface{}
}

// Message represent for message response
type Message struct {
	Msg string `json:"msg"`
}

// Response setting gin.JSON
func (g *Gin) Response(data *Data) {
	if data.Data != nil && data.Err == nil {
		if data.Code == 0 {
			data.Code = http.StatusOK
		}
		g.C.JSON(data.Code, data.Data)
		return
	}

	logger.Logger.Error(data.Err)

	if data.Code == 0 {
		data.Code = http.StatusInternalServerError
	}

	if data.Msg == "" {
		data.Msg = message.MsgFlags[data.Code]
	}

	g.C.JSON(data.Code, Message{Msg: data.Msg})
}

// CalculateSkipOffset ..
func CalculateSkipOffset(page, totalRecords, pageSize int) (offset int, take int, totalPages int) {
	totalPages = totalRecords / pageSize

	if totalRecords%pageSize > 0 {
		totalPages++
	}

	if page > totalPages {
		offset = 0
		take = 0
	} else {
		offset = (page - 1) * pageSize
		take = pageSize
	}
	return offset, take, totalPages
}
