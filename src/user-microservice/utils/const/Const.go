package cst

const (
	FIND_ALL         = "FIND_ALL_"
	FIND_BY_USERNAME = "FIND_BY_USERNAME_"

	AUTH_FAIL_COUNT = "AUTH_FAIL_COUNT_"
)
