package message

import "net/http"

var MsgFlags = map[int]string{
	http.StatusInternalServerError: "Something went wrong",
	http.StatusBadRequest:          "Bad request",
}

const (
	ERR_AUTH_USERNAME_PASSWORD_FAIL         = "Username or password is invalid"
	ERR_AUTH_USERNAME_PASSWORD_FAIL_TOO_MAX = "You are not allow to do this function due to you tried too many times"
	ERR_AUTH_ACCOUNT_DISABLED               = "Your account is disabled"
	ERR_AUTH_ACCOUNT_EXSITS                 = "Username already exists"

	ERR_USER_NOT_FOUND = "User not found"

	ERR_JWT_UNAUTHORIZED = "Token is unauthorized"
	ERR_JWT_EXPIRED      = "Token has expired"
	ERR_JWT_FORBIDDEN    = "Forbidden"
	ERR_JWT_INVALID      = "Token is invalid"
)
