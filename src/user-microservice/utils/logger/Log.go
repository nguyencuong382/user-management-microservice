package logger

import (
	"fmt"
	"os"
	"strings"
	"user-microservice/utils/setting"

	"github.com/sirupsen/logrus"
)

// Logger var
var Logger *logrus.Logger

func init() {
	Logger = logrus.New()
	Logger.Out = os.Stdout
	Logger.Formatter = &logrus.TextFormatter{ForceColors: true}
}

// Setup logger config
func Setup() {
	if strings.Compare(setting.AppSetting.LogFile, "") != 0 {
		file, err := os.OpenFile(setting.AppSetting.LogFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		if err == nil {
			Logger.Out = file
		} else {
			fmt.Println(err)
			Logger.Info("Failed to log to file, using default stderr")
		}
	}
}
