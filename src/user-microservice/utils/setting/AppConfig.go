package setting

import "time"

// App struct
type App struct {
	Cache             bool
	JwtSecret         string
	Env               string
	MigrateTable      bool
	MigrateData       bool
	DatabaseLogMode   bool
	JWTExpiredTime    int
	LogFile           string
	PageSize          int
	MaxPageSize       int
	MaxAuthFailed     int
	TimeOutAuthFailed int
}

// AppSetting setting obj
var AppSetting = &App{}

// Server struct
type Server struct {
	RunMode         string
	HTTPPort        int
	ReadTimeout     time.Duration
	WriteTimeout    time.Duration
	ShutDownTimeOut time.Duration
}

// ServerSetting setting obj
var ServerSetting = &Server{}

// Postgres struct
type Postgres struct {
	User     string
	Password string
	Host     string
	Port     int
	DBName   string
}

// PostgresSetting setting obj
var PostgresSetting = &Postgres{}

// Redis struct
type Redis struct {
	Host        string
	Password    string
	MaxIdle     int
	MaxActive   int
	IdleTimeout time.Duration
	DBNumber    int
}

// RedisSetting setting obj
var RedisSetting = &Redis{}
