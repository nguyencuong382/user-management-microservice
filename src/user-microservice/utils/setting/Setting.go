package setting

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/go-ini/ini"
)

var cfg *ini.File

// GetEnv : get env from .env file
func GetEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// Setup initialize the configuration instance for server
func Setup() {

	LoadAppFileConfig("app")

	mapTo("app", AppSetting)
	mapTo("server", ServerSetting)
	mapTo("postgres", PostgresSetting)
	mapTo("redis", RedisSetting)

	ServerSetting.ReadTimeout = ServerSetting.ReadTimeout * time.Second
	ServerSetting.WriteTimeout = ServerSetting.WriteTimeout * time.Second
	ServerSetting.ShutDownTimeOut = ServerSetting.ShutDownTimeOut * time.Second
	RedisSetting.IdleTimeout = RedisSetting.IdleTimeout * time.Second
}

// SetupTest initialize the configuration instance for test
func SetupTest() {
	LoadAppFileConfig("test")

	mapTo("test", TestSetting)
	mapTo("auth_test", AuthTestSetting)
}

// LoadAppFileConfig  load app config from ini file
func LoadAppFileConfig(prefix string) {
	var err error
	var fileEnv string

	pathToConfig := GetEnv("PATH_TO_CONFIG", "conf")
	AppSetting.Env = GetEnv("ENV", "local")
	if AppSetting.Env == "" {
		AppSetting.Env = "local"
	}
	fileEnv = fmt.Sprintf("%s/%s.%s.ini", pathToConfig, prefix, AppSetting.Env)
	log.Printf("Loading %s config %s", prefix, fileEnv)

	cfg, err = ini.Load(fileEnv)
	if err != nil {
		log.Fatalf("Fail to parse '%s': %v", fileEnv, err)
	}
}

// mapTo map section
func mapTo(section string, v interface{}) {
	err := cfg.Section(section).MapTo(v)
	if err != nil {
		log.Fatalf("Cfg.MapTo %s err: %v", section, err)
	}
}
