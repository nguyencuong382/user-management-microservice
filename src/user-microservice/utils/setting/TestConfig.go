package setting

// Test struct
type Test struct {
	URL string
}

// TestSetting setting obj
var TestSetting = &Test{}

// AuthTest struct
type AuthTest struct {
	AdminJWT string
	UserJWT  string
}

// AuthTestSetting setting obj
var AuthTestSetting = &AuthTest{}
