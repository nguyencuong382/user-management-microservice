package utils

import (
	"github.com/jinzhu/gorm"
)

// Exists return gorm error not found or boolean
func Exists(err error) (bool, error) {
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return false, nil
		}
		return false, err
	}

	return true, nil
}
