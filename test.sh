#!/bin/bash

export ENV=test.local
export GOPATH=`pwd` 
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
export PATH_TO_CONFIG=`pwd`/conf/user-microservice
go clean -testcache
./gotest -v --cover ./src/user-microservice/test... 